# **Leptos**
Leptos es un proyecto en desarrollo para la construcción de una estación de monitorización de la calidad del aire para ser desplegada sobre el terreno de modo autónomo. Está dotada del sensor Plantower 5003 de partículas sólidas en suspension, el sensor de gases multicanal de Seedstudio y un BME280 de temperatura, humedad y presión atmosférica.

Más información en https://imvec.tech

## **Listado de componentes**
- Placa de programación [ESP32-WROOM-32](https://imgs.search.brave.com/7VD6SlUk6tKr3AJ1LejNvT1b5xbSU_FzTq8Vgh6A0JM/rs:fit:474:225:1/g:ce/aHR0cHM6Ly90c2Ux/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5q/NTNoM3FDZXVnemQy/NjdGbUVQbTdBSGFI/YSZwaWQ9QXBp).
- Sensor de partículas sólidas en suspensión [Plantower 5003](https://imgs.search.brave.com/zstF0UBvZpcB10icoHu4o54i2HxCQue-RmVRSkBabbA/rs:fit:841:225:1/g:ce/aHR0cHM6Ly90c2Uz/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5S/d19xTkVGU01VNFdN/Vkk4QWxzQzRBSGFF/TCZwaWQ9QXBp).
- Sensor de gases [multicanal de Seedstudio](https://imgs.search.brave.com/uBvD8jTPya3E6toinjjufI1fapxcckl2fDCg4TbKh4Y/rs:fit:353:225:1/g:ce/aHR0cHM6Ly90c2Uy/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC5H/MkpFdnBKN0tuVTFG/Rm5lWUU5d1Z3SGFK/OCZwaWQ9QXBp).
- Reloj de Tiempo Real [DS3231](https://imgs.search.brave.com/PFvb14mAdmCqG_JeArKSYibILQyCMzQxn2oCGjxA7pU/rs:fit:474:225:1/g:ce/aHR0cHM6Ly90c2Uy/Lm1tLmJpbmcubmV0/L3RoP2lkPU9JUC44/bkNrNVUxVExQVlFR/dVF3MVBaUTRBSGFI/YSZwaWQ9QXBp9).
- Data logger [HW-125](https://m.media-amazon.com/images/I/616CeV67OHL._SX466_.jpg).


## **Esquema de montaje**
![](https://wiki.calafou.org/images/thumb/1/17/Leptos.png/1200px-Leptos.png)

## **Próximos pasos**
1. Añadir conectividad wifi.
2. Incorporar pantalla OLED. 
