// Leptos is an ongoing project developed whith love at the Arduinna lab of Calafou.org

#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "RTClib.h"
#include <Wire.h>
#include "MutichannelGasSensor.h"
#include <PMserial.h>

SerialPM pms(PMSx003, Serial);  // PMS5003 dust sensor
RTC_DS3231 rtc;                 // Real time clock 

  String print_time(DateTime timestamp) {    //Strings make managing data easily
  char message[120];

  int Year = timestamp.year();
  int Month = timestamp.month();
  int Day = timestamp.day();
  int Hour = timestamp.hour();
  int Minute = timestamp.minute();
  int Second= timestamp.second();

  sprintf(message, "%d-%d-%d %02d:%02d:%02d", Year, Month,Day,Hour,Minute,Second);
  
  return message;
}

//////// SD card //////////

void appendFile(fs::FS &fs, const char * path, const char * message){
    Serial.printf("Writing Leptos data to file: %s\n", path);

    File file = fs.open(path, FILE_APPEND);
    
    if(!file){
        Serial.println("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
        Serial.println("Data written correctly");
    } else {
        Serial.println("Writing failed");
    }
    file.close();
}

void setup(){
  Serial.begin(9600);
  gas.begin(0x04);    // Initialize gas sensor
  gas.powerOn();  
  pms.init();         // Initialize dust sensor
  
  if(!SD.begin()){
      Serial.println("Card Mount Failed");
      return;
      }

  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
    }

  if (!rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  appendFile(SD, "/leptos_log.txt", "Date and time - NO2 - CO- PM1 - PM2.5 - PM10\r\n");
}

void loop(){

  String dataString = "";

  float no2 = gas.measure_NO2();   // Values written to the SD card: 
  float co = gas.measure_CO();     // Date and time, Nitrogen dioxide (NO2), Carbon monoxide (CO) and PM1, PM2.5 and PM10 dust partic9/les
  float pm01 = pms.pm01;
  float pm25 = pms.pm25;
  float pm10 = pms.pm10;
  
  DateTime now = rtc.now();
  pms.read();                   // read the PM sensor

  dataString += String(print_time(now));  
  dataString += ",";
  dataString += String(no2);
  dataString += ",";
  dataString += String(co);
  dataString += ",";
  dataString += String(pm01);
  dataString += ",";
  dataString += String(pm25);
  dataString += ",";
  dataString += String(pm10);
  dataString += "\r\n";

Serial.print(now.year());
  Serial.print('/');
  Serial.print(now.month());
  Serial.print('/');
  Serial.print(now.day());
  Serial.print(" -- ");
  Serial.print(now.hour());
  Serial.print(':');
  Serial.print(now.minute());
  Serial.print(':');
  Serial.print(now.second());
  Serial.println();
  
  no2 = gas.measure_NO2();
  Serial.print("Concentration of NO2: ");
  if(no2>=0) Serial.print(no2);
  else Serial.print("invalid");
  Serial.println(" ppm");

  co = gas.measure_CO();
  Serial.print("Concentration of CO: ");
  if(co>=0) Serial.print(co);
  else Serial.print("invalid");
  Serial.println(" ppm");

  pm01 = pms.pm01;
  Serial.print("Concentration of PM1: ");
  if(pm01>=0) Serial.print(pm01);
  else Serial.print("invalid");
  Serial.println(" ug/m3");

  pm25 = pms.pm25;
  Serial.print("Concentration of PM25: ");
  if(pm25>=0) Serial.print(pm25);
  else Serial.print("invalid");
  Serial.println(" ug/m3");

  pm10 = pms.pm10;
  Serial.print("Concentration of PM10: ");
  if(pm10>=0) Serial.print(pm10);
  else Serial.print("invalid");
  Serial.println(" ug/m3");
  Serial.println();

  appendFile(SD, "/leptos_log.txt", dataString.c_str()); // Save data to SD card

  Serial.println();
  Serial.println();

      
  delay(3000);
}
